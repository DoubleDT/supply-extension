$(document).ready(() => {
    async function crawlAmazon() {
        let hostname = document.location.hostname;
        if (!hostname.includes('www.amazon.com')) {
            return;
        }
        let title = $('#productTitle').text().trim()
        let description = "No description"
        try {
            description = $('#productDescription')[0].outerHTML;
        } catch (error) {
            description = $('#feature-bullets')[0].outerHTML;
        }
        let price = $('#price_inside_buybox').text().replace('$', ' ').trim()
        if (!price) {
            price = $('#buyNew_noncbb').text().replace('$', ' ').trim()
        }
        price = Number(price)
        let specification = {};
        $('#productDetails_techSpec_section_1 tr').each(function () {
            let key = $(this).find("th").text().trim();
            let value = $(this).find("td:first").text().trim();
            specification[key] = value;
        })
        let images = []
        $('li.item').map(function () {
            let link = $(this).find('img').attr('src')
            link = link.replace("SS40", 'SY679')
            images.push(link)
        })
        images = images.slice(0, 5)
        let url = 'http://45.77.144.79/api/public/add-product-extension'
        $.post(url, {
            name: title,
            description,
            price,
            images: JSON.stringify(images),
            specification: JSON.stringify(specification)
        }, function (res) {

        })


    }
    crawlAmazon()
})
