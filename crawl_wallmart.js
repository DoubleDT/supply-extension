$(document).ready(() => {
    async function crawlWamart() {
        let title = $('h1.prod-ProductTitle').text().trim()
        let description = "No description"
        try {
            description = $('#product-about')[0].outerHTML;
        } catch (error) {
            description = $('#feature-bullets')[0].outerHTML;
        }
        let price = $('span.price-characteristic').attr('content').trim()
        price = Number(price)

        let specification = {};
        $('#specifications table tbody tr').each(function () {
            let key = $(this).find("td:first").text().trim();
            let value = $(this).find("td:eq(1) div").text().trim();
            specification[key] = value;
        })
        //console.log($('body')[0].outerHTML)
        let { item } = JSON.parse($('script#item').text());
        console.log(item.product.buyBox.products)
        let images = []
        $('li.item').map(function () {
            let link = $(this).find('img').attr('src')
            link = link.replace("SS40", 'SY679')
            images.push(link)
        })
        images = images.slice(0, 5)
        let url = 'http://demo-supply.site/api/public/add-product-extension'
        $.post(url, {
            name: title,
            description,
            price,
            images: JSON.stringify(images),
            specification: JSON.stringify(specification)
        }, function (res) {

        })


    }
    crawlWamart()

})