$(document).ready(() => {
    async function crawlWayfair() {
        let hostname = document.location.hostname;
        if (!hostname.includes('www.wayfair.com')) {
            console.log('sds')
            return;
        }
        let title = $('h1.pl-PageTitle-header').text().trim()
        let description = "No description"
        description = $('.ProductOverviewInformation-content')[0].outerHTML;
        // console.log(description)
        let price = document.querySelector('.notranslate').textContent;
        price = Number(price.slice(1))
        let specification = {};
        $('.pl-DescriptionList').each(function () {
            $(this).find('dt').each(function () {
                let key = $(this).text();
                let value = $(this).next().text()
                specification[key] = value;
            })

        })
        let images = []
        $('.ImageComponent.ImageComponent--overlay').map(function () {
            let link = $(this).find('img').attr('src')
            images.push(link)
        })
        images = images.slice(0, 5)
        // let url = 'http://mallfairs.com/api/public/add-product-extension';
        let url = 'http://localhost:4000/api/public/add-product-extension';

        $.post(url, {
            name: title,
            description,
            price,
            images: JSON.stringify(images),
            specification: JSON.stringify(specification)
        }, function (res) {
            if (res.success) {
                alert('Success')
            }
        })


    }
    crawlWayfair()

})
